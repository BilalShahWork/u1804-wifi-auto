# u1804-wifi-auto
Automate the installation of wifi interfaces on the laptop after installing ubuntu 18.04 from USB  

# curent approach 
Use [netplan](https://netplan.io/reference)  to implement wifi and learn about [wpa_supplicant](https://en.wikipedia.org/wiki/Wpa_supplicant) --- start with this [tutorial](https://www.linuxbabe.com/command-line/ubuntu-server-16-04-wifi-wpa-supplicant)
and also explore using [NetworkManager](https://en.wikipedia.org/wiki/NetworkManager#Mobile_broadband_configuration_assistant) instead of *systemd-networkd* therefore replacing wpa_supplicant
- [Network Manager in Ubuntu](https://help.ubuntu.com/community/NetworkManager)  
- [How to Connect Wi-Fi from Linux Terminal Using Nmcli Command](https://www.tecmint.com/nmcli-connect-wi-fi-from-linux-terminal/)  <-- works like a charm .. look f roall his other interesting tutorials (Aaron Kili)   
- [nmcli in Ubuntu 18.04](http://manpages.ubuntu.com/manpages/cosmic/man1/nmcli.1.html#see%20also)
- [nmtui in Ubuntu 18.04](https://www.hiroom2.com/2018/05/29/ubuntu-1804-network-en/)

## Bummers:
- [Cloud-init does not support wifi types - documented here](https://cloudinit.readthedocs.io/en/latest/topics/network-config-format-v2.html#network-config-v2)  --- so we use netplan 
- [netplan documentstion](https://launchpad.net/netplan)
- [similar efforts 1](https://gist.github.com/tcoupin/c8a735b6eab83fa41a104d919dc49e88)
- [similar efforts 2](https://gitlab.com/search?utf8=%E2%9C%93&search=setup+wifi+with+cloud-init&group_id=&project_id=&repository_ref=)
- [similar efforts 3](https://gitlab.com/Bjorn_Samuelsson/raspberry-pi-cloud-init-wifi)
- [similar efforts 4](https://gitlab.com/hacklab808/raspberry-pi-cloud-init-wifi)

# exploring cloud config ....references
- [main website](https://cloud-init.io/)
- [source ubuntu launchpad](https://launchpad.net/ubuntu/+source/cloud-init)
- [source launchpad](https://code.launchpad.net/cloud-init)
- [main documentation](http://cloudinit.readthedocs.org)
- [how to contribute](http://cloudinit.readthedocs.io/en/latest/topics/hacking.html)
- [latest doc](https://cloudinit.readthedocs.io/en/latest/)
- [great examples](https://www.scaleway.com/en/docs/how-to-use-cloud-init-to-configure-your-server-at-first-boot/)
- [examples with ubuntu](https://www.ibm.com/support/knowledgecenter/en/SSB27U_6.4.0/com.ibm.zvm.v640.hcpo5/instsubuntu.htm)
- [offline cloud-init](https://serverfault.com/questions/897610/are-there-any-examples-of-using-cloud-init-offline-or-without-cloud-hosting)
- [in IBM zVM](https://www.ibm.com/support/knowledgecenter/en/SSB27U_6.4.0/com.ibm.zvm.v640.hcpo5/instcloud.htm)
- #### [local use of cloud init](http://www.whiteboardcoder.com/2016/04/install-cloud-init-on-ubuntu-and-use.html)
- [seeding data](https://github.com/number5/cloud-init/tree/master/doc/examples/seed)