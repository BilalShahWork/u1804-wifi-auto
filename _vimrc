" ---------------------------------------------------------------------------------
" some cool links for vi indenting 
"   indenting:      https://www.cs.oberlin.edu/~kuperman/help/vim/indenting.html
"   reformatting:   https://www.cs.oberlin.edu/~kuperman/help/vim/reformatting.html
" ---------------------------------------------------------------------------------
syntax on               " turn on syntax detection
filetype plugin indent on   " auto formatting on detecting filetype
colorscheme murphy      " my favorite colorscheme with dark background 
set nu                  " set numbers on 
set guifont=6x13        " my favorite font - must be available on local machine 
" ---- indenting ---- https://www.cs.oberlin.edu/~kuperman/help/vim/indenting.html
" To indent the current line, or a visual block:
" ctrl-t, ctrl-d  - indent current line forward, backwards  (insert mode)
" visual > or <   - indent block by sw (repeat with . )
" To stop indenting when pasting with the mouse, add this to your .vimrc:
" :set pastetoggle=<f5>
" then try hitting the F5 key while in insert mode (or just :set paste).
" 
" ---------------------------------------------------------------------------------
set autoindent         " auto indent
set expandtab        " expand tabs to spaces
set smartindent " smart indent
set cindent        " C/C++ indents
set cin         " -
" by default, the indent is 2 spaces. 
set shiftwidth=4        " spaces in indent
set softtabstop=4
set tabstop=4
"   set nuw=4   " indentation while in indent and pressing newline to start a line 
"
" for html/rb files, 2 spaces
"
autocmd FileType html setlocal ts=2 sw=2 expandtab
autocmd FileType ruby setlocal ts=2 sw=2 expandtab
"
" for js/coffee/jade files, 4 spaces
"
autocmd FileType javascript setlocal ts=4 sw=4 sts=0 expandtab
autocmd FileType coffeescript setlocal ts=4 sw=4 sts=0 expandtab
autocmd FileType jade setlocal ts=4 sw=4 sts=0 expandtab
"
" for YAML files
"
autocmd FileType yaml setlocal ts=2 sw=2 sts=2 expandtab
"
" save as unix endofline chars rather than DOS
"
set fileformat=unix
