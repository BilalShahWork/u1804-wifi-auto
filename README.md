# u1804-wifi-auto
Automate the installation of wifi interfaces on the laptop after installing ubuntu 18.04 from USB  
 - use older installer an dit provides fo rnetwork install using the wifi
   - [lack of features in new installer - the blog](https://askubuntu.com/questions/1032644/ubuntu-server-18-04-setting-wifi-connections-when-install)
   - [lack of features in new installer - the bug](https://bugs.launchpad.net/ubuntu/+source/subiquity/+bug/1750645)
   - [all alternative downloads](https://www.ubuntu.com/download/alternative-downloads#alternate-ubuntu-server-installer)
   - [ubuntu server 18.04.02 - alternative download](http://cdimage.ubuntu.com/releases/18.04/release/)

 - [This is the older research trying to do it with new installer of ubuntu](./README-old1.md) 
