# -----------------------------------------------------------------------------
# Author: Bilal_Shah@yahoo.com
# 
# remote executed from git ... no shebang required here 
# bash <(curl https://gitlab.com/BilalShahWork/u1804-wifi-auto/raw/master/setup.sh?inline=false)
# the link comes from the download button for the file in gitlab repo  
# reference:
# http://stackoverflow.com/questions/4642915/passing-parameters-to-bash-when-executing-a-script-fetched-by-curl
# curl http://foo.com/script.sh | bash -s arg1 arg2
# 
# This is the setup for the seeding the cloud-init instance locally 
# to achieve a desired customization , originally motivated to include wifi network configurationi.
# There are some assumptions below and they will materialize as we progress:
#
# -) Ubuntu 18.04 has just been installed - use the traditional installer which includes wifi but 
#    not cloud-init 
#    URL https://www.ubuntu.com/download/alternative-downloads#alternate-ubuntu-server-installer
# -) there is a working git setup on this system - it is configured here 
# -) The userid "user" is present - configured during the ubuntu install
# -) run as root 
# -----------------------------------------------------------------------------
#
# initial update and upgrade - may even throw up menu 
#
apt -y update && apt -y upgrade
#
# configure git
#
export "HOME=/root"
git config --global user.name "Bilal Shah"
git config --global user.email bilal_shah@yahoo.com
git config --global color.diff auto
git config --global color.status auto
#
# deep purge the files - to get new packages to install 
#
rm -rf /var/lib/cloud/*
#
# light purge the files 
#
rm -rf /var/log/cloud-init*
rm -rf ~/gitrepos
mkdir -p ~/gitrepos
cd ~/gitrepos
#
# get the repo 
#
git clone https://gitlab.com/BilalShahWork/u1804-wifi-auto.git
cd u1804-wifi-auto 
#
# install cloud-init 
#
apt -y install cloud-init
# - no need to do this when apt install .... do it on source code install 
# - cloud-init init --local
# - cloud-init status
# - ln -s /usr/local/bin/cloud-init /usr/bin/cloud-init
# - for svc in cloud-init-local.service cloud-init.service cloud-config.service cloud-final.service
# - do
# -   systemctl enable $svc
# -   #systemctl start  $svc
# - done
#
# stage the seeding files
#
echo "local-hostname: `hostname`" > meta-data
echo "instance-id: my-test-instance-1" >> meta-data
cp _vimrc ~
mkdir -p /var/lib/cloud/seed/nocloud-net
for i in user-data meta-data network-config
do
    cp $i /var/lib/cloud/seed/nocloud-net
    chmod 0600 /var/lib/cloud/seed/nocloud-net/$i
    chown root:root /var/lib/cloud/seed/nocloud-net/$i
done
#
# it all come together here --- look in the 
# /var/log/cloud-init.log           -- for activity 
# /var/log/cloud-init-output.log    -- for output :) 

#
# adding 2 parameters to the kernel boot params
# consoleblank=30 - turns off the screen while server is on after 30 sec
# nouveau.modeset=0 - allow sto overcome a bug in recovering after lid closure
#
# TODO need an if structure around the block below
# TODO revert the /etc/systemd/logind.conf file 
# TODO noveau.modeset - not effective - laptopo still stuck in consoleblank
# +---------------------------------------------------------------------------------------------------
# | sed -i.orig -e 's/^GRUB_CMDLINE_LINUX_DEFAULT.*/GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"/gm' /etc/default/grub
# | update-grub
# | This is one way to put consoleblank in the kernel boot params ... but there was a problem.  when 
# | the lid was closed then the screen would stay blank ... though teh backlight would come up 
# | ... plain ol setterm --blank <minutes>  worked much better than that 
# +---------------------------------------------------------------------------------------------------

if [ -f /etc/issue.original ]
then
    echo "setterm already provisioned .... skipping"
else
    cp /etc/issue /etc/issue.original
    cat setterm-blank-1-saved-code > /etc/issue
    cat /etc/issue.original >> /etc/issue
fi

reboot now

# -> notes - 
# -> notes - 
# -> notes - root@e4300:~# man grep
# -> notes - root@e4300:~# grep -e "consoleblank" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank" -e "nouveau" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank=" -e "nouveau.modeset=0" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]" -e "nouveau.modeset=0" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]+" -e "nouveau.modeset=0" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "modeset=0" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub 
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=90" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# grep -e "consoxleblank=[0-9]." -e "noxuveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - 1
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# grep -e "consoleblank=[0-9]." -e "nouveau.modeset=0" /etc/default/grub ; echo $?
# -> notes - GRUB_CMDLINE_LINUX_DEFAULT="consoleblank=30 nouveau.modeset=0"
# -> notes - 0
# -> notes - root@e4300:~# man test
# -> notes - root@e4300:~# man grep
# -> notes - root@e4300:~# test
# -> notes - 
# -->setterm --blank 1 -- in /etc/profile.d/screensaver.sh
# -->use root@e6420:~# cat /sys/module/kernel/parameters/consoleblank  -- to check the value 
# -->*** the final solution ***
# -->https://unix.stackexchange.com/questions/8056/disable-screen-blanking-on-text-console
# ****************************************************************************************************
# ****************  Working solution - adapted form site https://unix.stackexchange.com/questions/3759/how-to-stop-cursor-from-blinking
# save doff the code for --> setterm --blank 1 <-- into a file and use that to prepend /etc/issue
# sudo cp /etc/issue /etc/issue.tmp
# setterm --blank 1 | sudo tee /etc/issue
# cat /etc/issue.tmp | sudo tee --append /etc/issue
# sudo rm /etc/issue.tmp
# sudo reboot
#
# ****************************************************************************************************
